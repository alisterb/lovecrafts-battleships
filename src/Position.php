<?php
namespace Battleships;

/**
 * Value object to hold a co-ordinate.
 *
 * as such a simple value object, we'll just access the vars directly
 */
class Position
{
    public $x;
    public $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }
}
