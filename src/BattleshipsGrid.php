<?php
namespace Battleships;

class BattleshipsGrid
{
    const DEFAULT_GRID_SIZE = 10;

    private $horizontal;
    private $vertical;

    /**
     * A reference to a particular ship at a position
     * 
     * @var array array of ShipsSections at (x,y) positions
     */
    private $grid;

    /**
     * How many sections have been placed?
     *
     * @var integer
     */
    private $sectionsPlaced = 0;

    /**
     * The count of sections destroyed so far
     *
     * When $sectionsDestroyed === $sectionsPlaced - you win.
     *
     * @var integer
     */
    private $sectionsDestroyed = 0;

    public function __construct(
        $horizontal = self::DEFAULT_GRID_SIZE,
        $vertical = self::DEFAULT_GRID_SIZE
    ) {
        // could also validate both params are >2 and <= 200?

        $this->horizontal = $horizontal;
        $this->vertical = $vertical;
    }

    public function isGameCompleted()
    {
        return $this->countOfHits() === $this->sectionsPlaced;
    }

    public function countOfHits()
    {
        return $this->sectionsDestroyed;
    }

    /**
     * attempt an earth-shattering kaboom against a Position
     *
     * @param Position $pos VO of the coordinates to try
     *
     * @return Ship|false Ship that was hit, or no-hit
     */
    public function target(Position $pos)
    {
        if (! isset($this->grid[$pos->x][$pos->y])) {
            // no hit against a section.
            return false;
            // I could have used some sort of NullShip here, but it's easy enough as it is for now.
        }

        // find out what part of the ship was hit.
        $shipSection = $this->grid[$pos->x][$pos->y];

        $this->sectionsDestroyed ++;
        $shipSection->hit();

        return $shipSection->ship;
    }

    public function placeShip(Ship $ship)
    {
        foreach ($ship->allSections() as $section => $sectionPosition) {
            $this->placeSectionAt($sectionPosition, $ship, $section);
            $this->sectionsPlaced ++;
        }
        return true;
    }

    public function placeSectionAt(Position $pos, Ship $ship, $section)
    {
        $this->validatePosition($pos);
        $shipSection = new ShipSection($ship, $section);
        $this->grid[$pos->x][$pos->y] = $shipSection;
    }

    /**
     * Is it valid to put (part of) a ship at this position
     *
     * @param Position $pos [description]
     *
     * @throws LogicException       Already has a ship there
     * @throws OutOfBoundsException Too far down
     */
    public function validatePosition(Position $pos)
    {
        if (isset($this->grid[$pos->x][$pos->y])) {
            throw new \LogicException("There is a ship already at that position");
        }

        // check position is in bounds (if they are === to max, it's too high, #ZeroBase)
        if ($pos->x >= $this->horizontal) {
            throw new \OutOfBoundsException("X position is too high.");
        }
        if ($pos->y >= $this->vertical) {
            throw new \OutOfBoundsException("Y position is too high.");
        }

        return;
    }

    // @codeCoverageIgnoreStart
    public function displayGrid()
    {
        echo "\nGrid is {$this->horizontal} x {$this->vertical}\n";

        for ($y = 0; $y < $this->vertical; $y ++) {
            for ($x = 0; $x < $this->horizontal; $x ++) {
                if (isset($this->grid[$x][$y])) {
                    $shipSection = $this->grid[$x][$y];
                    $ship        = $shipSection->ship;
                    echo ($shipSection->isHit ? 'x' : $ship->getSize());
                } else {
                    echo '.';
                }
            }
            echo "\n";
        }
        echo "Done\n";
    }
    // @codeCoverageIgnoreEnd
}
