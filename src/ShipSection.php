<?php
namespace Battleships;

use Battleships\Position;

/**
 * One 'segment' of a ship.
 *
 * It knows what ship it belongs to, which segment of the ship
 *
 * used as a proxy between the grid, which contains these sections, and the ship itself
 */
class ShipSection
{
    public $ship;
    public $section;
    public $isHit = false;

    public function __construct(Ship $ship, $section)
    {
        $this->ship    = $ship;
        $this->section = $section;
    }

    /**
     * record a hit against the ship
     *
     * @param [type] $section [description]
     *
     * @return [type] [description]
     */
    public function hit()
    {
        if ($this->isHit) {
            throw new \RuntimeException("Section has already been blowed up!");
        }

        $this->isHit = true;
        $this->ship->hit($this->section);
    }
}
