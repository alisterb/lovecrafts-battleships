<?php
namespace Battleships;

use Battleships\Position;

/**
 * The Head position and direction of a ship.
 *
 * It knows how many times it has been hit (and so if it has been destroyed)
 * and can calculate from the start position and direction, where the other
 * sections/segments of the ship would be.
 *
 * If I'd created a NullShip, it would implement an interface and then return
 * 'safe' defaults.
 *   * public function hit($section)      { ; //'NO-OP' }
 *   * public function getHitCount()      { return 0; }
 *   * public function isDestroyed()      { return false; }
 *   * public function getHitPercentage() { return 0.0; }
 *   * public function getSize()          { return 0; }
 *   * public function allSections()      { return []]; }
 */
class Ship  // implements ShipInterface -- if I wanted a NullShip
{
    const DIR_HORIZONTAL = 'h';
    const DIR_VERTICAL   = 'v';

    private $size;
    private $position;
    private $direction;

    /**
     * Array of hit-statuses, 0 if not hit, 1 if it's been hit
     *
     * We use 0 and 1 so we can just count the 1's for the number of hits
     *
     * @var integer[]
     */
    private $sectionsHit;

    public function __construct($size, Position $position, $direction)
    {
        $this->validate($size, $position, $direction);
        $this->size      = $size;
        $this->position  = $position;
        $this->direction = $direction;
        for ($i = 0; $i < $this->size; $i ++) {
            $this->sectionsHit[$i] = 0;
        }
    }

    /**
     * Ensure the parameters make sense.
     *
     * @param integer  $size      How many pieces make up the ship [2...6]
     * @param Position $position  Where the ship's head is.
     * @param char     $direction 'H' or 'V', best to use the constants
     *
     * @return [type]   [description]
     */
    public function validate($size, Position $position, $direction)
    {
        if ($size < 2) {
            throw new \OutOfBoundsException("Ship size is too small");
        }

        if ($size > 6) {
            throw new \OutOfBoundsException("Ship size is too large");
        }

        if ($position->x < 0 || $position->y < 0) {
            // we don't know how large the grid is, so we leave that to when it is being placed
            throw new \OutOfBoundsException("Position must be on the grid");
        }

        if (! in_array($direction, [self::DIR_HORIZONTAL, self::DIR_VERTICAL])) {
            throw new \InvalidArgumentException("Direction must be horizontal, or vertical. Use the constants");
        }

        return;
    }

    /**
     * record a hit against the ship
     *
     * @param integer $section 
     */
    public function hit($section)
    {
        $this->sectionsHit[$section] = 1;
    }

    /**
     * Number of sections that have been hit.
     *
     * @return integer
     */
    public function getHitCount()
    {
        $x = array_sum($this->sectionsHit);
        return $x;
    }

    /**
     * Have all the parts been hit?
     *
     * @return boolean
     */
    public function isDestroyed()
    {
        return $this->getHitCount() === $this->size;
    }

    /**
     * Simplistic - it won't like floating points like 1/3 section hit.
     *
     * @return float Percentage of the nuclear wessel that has been hit.
     */
    public function getHitPercentage()
    {
        return ($this->getHitCount() / $this->size) * 100;
    }

    /**
     * How big is the ship?
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Return an arroy of positions for all segments of the ship.
     *
     * @return Position[] All sections of the ship
     */
    public function allSections()
    {
        $positions = array (0 => $this->position, );

        $nextPosition = clone($this->position);
        // we have already stored the first position, continue from section #1
        for ($section = 1; $section < $this->size ; $section ++) {

            if ($this->direction === self::DIR_HORIZONTAL) {
                $nextPosition->x ++;
            } else {
                $nextPosition->y ++;
            }

            $positions[$section] = clone($nextPosition);
        }
        return $positions;
    }
}



    
    
    
    
    
    
