# Introduction


The classic game of battleships has the following rules:

* Setup: ​Each player has a board with two grids, one of each type of ship, and a bunch of hit and miss markers. Pen and paper players should note there is one length 2 ship, two length 3 ships, one length 4 ship, and one length 5 ship.

Secretly place your ships on the lower grid. Each ship must be placed horizontally or vertically (not diagonally) across grid spaces, and can't hang over the grid. Ships can touch each other, but can't both be on the same space.

* Play: ​Players take turns firing a shot to attack enemy ships.

On your turn, call out a letter of a row and number of a column on the grid. Your opponent checks that 
space on their lower grid, and says "miss" if there are no ships there, or "hit" if you guessed a space that contained a ship.

Mark your shots on your upper grid, with white pegs for misses and red pegs for hits, to keep track of your guesses.

When one of your ships is hit, put a red peg into that ship on your lower grid at the location of the hit. Whenever one of your ships has every slot filled with red pegs, you must announce to your opponent that he has sunk your ship.

* Victory​: The first player to sink all opposing ships wins.

 <http://boardgames.about.com/od/battleship/a/Rules-of-Battleship.htm>


# Coding exercise (PHP)


Design a set of classes to support a game of battleships.

NOTE​: Do not implement the complete game. Only the components and behaviours described
below (some of which modify the classic rules) should be implemented. 

The participants that should be present are:

1. a component that represents a grid of arbitrary size
2. a configurable number of ship components ranging from scout size (two units), to aircraft carrier size (six units)

The grid should be coded so that it allows the ‘placement’ of ships within it and observes these constraints: 

1. ships can lie horizontally or vertically (not diagonally)
2. ships are not allowed to overlap
3. ships must be placed fully within the grid

The grid should accept targeted artillery rounds and report/track the following:

1. hit or miss
2. ship sink (when every square of a ship has been hit)
3. score: number of shots, number of hits, number of sinks

Ships should also manage their own state. In particular

1. size
2. percentage hit
3. destroyed or not

For this exercise, do not assume persistence between processes.

Write a suite of tests that demonstrate

1. placement of ships on the grid
2. several hits
3. several misses
4. a sinking
5. error handling when ship placed illegally
6. expected score values
7. reported status of a ship (talking directly to the ship rather than to the grid) confirming
   7.1. hit yet?
   7.2. sunk yet?
   7.3. percentage hit

# Required:

## Grid

* Arbitrary rectangle X*Y
* Ship position, direction (and/or end-point)
* Which ship is on which point service (ShipSection)
  * on initial placement grid loops over all possible section points
  * confirm point is valid
  * confirm point is empty
* Stats
  * Hits
  * misses

## Ship

* Ship sized from 2 to 6
* Placement at X/Y position and direction
* Ship sections hit/unhit
* % hit
* destroyed?
* Service to check 
  * Valid Placement of ships on the grid, H or V
  * Non-overlapping ships
