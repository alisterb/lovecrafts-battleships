<?php
namespace Tests\Battleships;

use Battleships\BattleshipsGrid;
use Battleships\Position;
use Battleships\Ship;

class GameTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->grid = new BattleshipsGrid(10, 10);

        // a grid with a 2-up ship already in place at [0,0]->[0,1], Horizontally
        $this->sampleGrid = new BattleshipsGrid(10, 10);

        $this->sampleShip = new Ship(2, new Position(0, 0), Ship::DIR_HORIZONTAL);
        $this->sampleGrid->placeShip($this->sampleShip);
    }

    /**
     * 1. placement of ships on the grid
     */
    public function testPlacementOfShipsOnTheGrid()
    {
        $ship1 = new Ship(2, new Position(0, 0), Ship::DIR_VERTICAL);
        $this->grid->placeShip($ship1);

        $ship2 = new Ship(3, new Position(1, 0), Ship::DIR_HORIZONTAL);
        $this->assertTrue($this->grid->placeShip($ship2));
        //#$this->grid->displayGrid();
    }

    /**
     * 2. several hits (to a 5-up)
     */
    public function testSeveralHits()
    {
        $ship1 = new Ship(5, new Position(0, 0), Ship::DIR_VERTICAL);
        $this->grid->placeShip($ship1);
        //#$this->grid->displayGrid();

        $ship = $this->grid->target(new Position(0,0));
        $this->assertInstanceOf('Battleships\Ship', $ship);

        $ship = $this->grid->target(new Position(0,1));
        $this->assertInstanceOf('Battleships\Ship', $ship);

        $ship = $this->grid->target(new Position(0,2));
        $this->assertInstanceOf('Battleships\Ship', $ship);

        $ship = $this->grid->target(new Position(0,3));
        $this->assertInstanceOf('Battleships\Ship', $ship);

        $this->assertFalse($ship->isDestroyed());
        //#$this->grid->displayGrid();
    }
    
    /**
     * 3. several misses
     *
     * We aren't recording where a target/miss occurred.
     * The spec didn't say to do so....
     */
    public function testSeveralMisses()
    {
        $ship1 = new Ship(5, new Position(0, 0), Ship::DIR_VERTICAL);
        $this->grid->placeShip($ship1);
        //#$this->grid->displayGrid();

        $this->assertFalse($this->grid->target(new Position(6,6)));
        $this->assertFalse($this->grid->target(new Position(7,7)));
        $this->assertFalse($this->grid->target(new Position(8,8)));
        $this->assertFalse($this->grid->target(new Position(0,8)));

        $this->assertFalse($ship1->isDestroyed());  // nowhere near
        //#$this->grid->displayGrid();
    }

    /**
     * 4. a sinking
     *
     * Use the 2-up on the sampleGrid
     */
    public function testSinkASmallCraft()
    {
        //#$this->sampleGrid->displayGrid();

        $target1 = $this->sampleGrid->target(new Position(0,0));
        $this->assertInstanceOf('Battleships\Ship', $target1);
        $target2 = $this->sampleGrid->target(new Position(1,0));
        $this->assertInstanceOf('Battleships\Ship', $target2);

        $this->assertTrue($target2->isDestroyed());
        $this->assertTrue($this->sampleShip->isDestroyed());
        // the targets we get back are the original ship we placed.
        $this->assertSame($target2, $this->sampleShip);
    }

    /**
     * 5a. error handling when ship placed illegally (on top of another ship)
     *
     * @expectedException LogicException
     */
    public function testErrorWhenPlacedShipOnAnother()
    {
        $ship1 = new Ship(2, new Position(0, 0), Ship::DIR_VERTICAL);
        $ship2 = new Ship(2, new Position(0, 0), Ship::DIR_VERTICAL);

        $this->grid->placeShip($ship1);
        $this->grid->placeShip($ship2);
    }

    /**
     * 5b. error handling when ship placed illegally (out of bounds)
     *
     * @expectedException OutOfBoundsException
     */
    public function testErrorWhenPlacedShipOutOfBoundsStartLocation()
    {
        $ship1 = new Ship(2, new Position(-1, -1), Ship::DIR_VERTICAL);
        $this->grid->placeShip($ship1);
    }

    /**
     * 5c. error handling when ship placed illegally (out of bounds)
     *
     * @expectedException OutOfBoundsException
     * @expectedExceptionMessage Y position is too high.
     */
    public function testErrorWhenPlacedShipOutOfBoundsTrailingLocationTooFarDown()
    {
        // try to place a ship so it would drop off the bottom of grid
        $ship1 = new Ship(5, new Position(0, 9), Ship::DIR_VERTICAL);
        $this->grid->placeShip($ship1);
    }

    /**
     * 5c. error handling when ship placed illegally (out of bounds)
     *
     * @expectedException OutOfBoundsException
     * @expectedExceptionMessage X position is too high.
     */
    public function testErrorWhenPlacedShipOutOfBoundsTrailingLocationTooFarRight()
    {
        // try to place a ship so it would drop off the bottom of grid
        $ship1 = new Ship(5, new Position(9, 0), Ship::DIR_HORIZONTAL);
        $this->grid->placeShip($ship1);
    }

    /**
     * 6. expected score values
     */
    public function testScoreValues()
    {
        $this->assertFalse($this->sampleGrid->isGameCompleted());

        $this->assertEquals(0, $this->sampleGrid->countOfHits());
        $target1 = $this->sampleGrid->target(new Position(0,0));
        $this->assertEquals(1, $this->sampleGrid->countOfHits());
        $target2 = $this->sampleGrid->target(new Position(1,0));

        $this->assertSame($target2, $this->sampleShip);
        $this->assertTrue($target2->isDestroyed());

        $this->assertEquals(2, $this->sampleGrid->countOfHits());
        $this->assertTrue($this->sampleGrid->isGameCompleted());
    }

    // {{{  7. reported status of a ship (talking directly to the ship rather than to the grid):

    /**
     * 7.1. hit yet?
     */
    public function testShipStatusHit()
    {
        $ship = $this->sampleGrid->target(new Position(0,0));
        // hit at [0,0]
        $this->assertInstanceOf('Battleships\Ship', $ship, "Expected to get a ship that was hit");
        $this->assertFalse($ship->isDestroyed());
    }

    /**
     * 7.2. sunk yet?
     */
    public function testShipStatusSunk()
    {
        $ship = $this->sampleGrid->target(new Position(0,0));
        $this->assertFalse($ship->isDestroyed());

        // hit at [0,0]
        $this->assertInstanceOf('Battleships\Ship', $ship);
        $this->assertFalse($ship->isDestroyed());  // just winged it so far

        $ship = $this->sampleGrid->target(new Position(1,0));
        // another hit
        $this->assertInstanceOf('Battleships\Ship', $ship);

        // it's sunk
        $this->assertTrue($ship->isDestroyed());
    }

    /**
     * 7.3. percentage hit
     */
    public function testShipStatusPercentageHit()
    {
        $ship = $this->sampleGrid->target(new Position(0,0));
        $this->assertFalse($ship->isDestroyed());
        $this->assertEquals(50, $ship->getHitPercentage());

        $ship = $this->sampleGrid->target(new Position(1,0));
        $this->assertEquals(100, $ship->getHitPercentage());
        $this->assertTrue($ship->isDestroyed());
        //#$this->sampleGrid->displayGrid();
    }

    // }}}
}
