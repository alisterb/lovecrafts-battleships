<?php
namespace Tests\Battleships;

use Battleships\Ship;
use Battleships\Position;

class ShipTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->ship = new Ship(2, new Position(0, 0), Ship::DIR_HORIZONTAL);
    }

    /**
     * @small
     */
    public function testSanity()
    {
        $this->assertInstanceOf('Battleships\Ship', $this->ship);
    }

    /**
     * for code coverage completion
     * 
     * @small
     */
    public function testGetSize()
    {
        $this->assertEquals(2, $this->ship->getSize());
    }

    /**
     * @expectedException \OutOfBoundsException
     */
    public function testInvalidSizeSmall()
    {
        $this->ship = new Ship(1, new Position(0, 0), Ship::DIR_HORIZONTAL);
    }

    /**
     * @expectedException \OutOfBoundsException
     */
    public function testInvalidSizeLarge()
    {
        $this->ship = new Ship(9, new Position(0, 0), Ship::DIR_HORIZONTAL);
    }

    /**
     * @expectedException \OutOfBoundsException
     */
    public function testInvalidPosition()
    {
        $this->ship = new Ship(2, new Position(-1, 0), Ship::DIR_HORIZONTAL);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidDirection()
    {
        $this->ship = new Ship(2, new Position(0, 0), 'x');
    }

   /**
     * Test we have a test ship in the correct locations
     * 
     * @small
     */
     public function testAllSections()
    {
        $this->assertEquals(
            [
                new Position(0,0),
                new Position(1,0),
            ], 
            $this->ship->allSections()
        );
    }
}
