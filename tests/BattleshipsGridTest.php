<?php
namespace Tests\Battleships;

use Battleships\BattleshipsGrid;
use Battleships\Position;
use Battleships\Ship;

class BattleshipsGridTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->grid = new BattleshipsGrid(10, 10);
    }

    /**
     * @small
     */
    public function testSanity()
    {
        $this->assertInstanceOf('Battleships\BattleshipsGrid', $this->grid);
    }

    /**
     * [testCreateWithOtherSize description]
     *
     * @small
     */
    public function testCreateWithOtherSize()
    {
        $grid1 = new BattleshipsGrid(3, 3);
        $this->assertInstanceOf('Battleships\BattleshipsGrid', $grid1);

        $grid2 = new BattleshipsGrid(100, 100);
        $this->assertInstanceOf('Battleships\BattleshipsGrid', $grid2);
    }

    /**
     * @dataProvider dataproviderPlaceShip
     */
    public function testPlaceShip(Ship $ship)
    {
        try {
            $this->grid->placeShip($ship);
        } catch (\Exception $e) {

            $this->grid->displayGrid();
            var_dump($ship);
            var_dump($e);
            die;
            $this->fail('An exception was raised while placing a ship.');
        }
        $this->assertTrue(true); // ship was placed OK.
    }

    // returns a Ship, with embedded placement
    public static function dataproviderPlaceShip()
    {
        $validShip = array(
            // size, x, y, direction
            [2, new Position(0, 0), Ship::DIR_HORIZONTAL],
            [3, new Position(0, 1), Ship::DIR_HORIZONTAL],
            [4, new Position(0, 2), Ship::DIR_HORIZONTAL],
            [6, new Position(4, 1), Ship::DIR_VERTICAL ],
        );

        foreach ($validShip as $s) {
            $ship = new Ship($s[0], $s[1], $s[2]);
            yield array($ship);
        }
    }

    /**
     * Attempt to place two ships in the same space
     * 
     * @expectedException LogicException
     */
    public function testPlaceShipOnTop()
    {
        $ship1 = new Ship(2, new Position(0, 0), Ship::DIR_VERTICAL);
        $this->grid->placeShip($ship1);

        $ship2 = new Ship(3, new Position(0, 0), Ship::DIR_HORIZONTAL);
        $this->grid->placeShip($ship2);
    }

    public function testTargetingMisses()
    {
        // fill the grid ready to test hitting things
        $ship = new Ship(2, new Position(0, 0), Ship::DIR_HORIZONTAL);
        $this->grid->placeShip($ship);  // OK, just one ship

        #$this->grid->displayGrid();
        $this->assertFalse($this->grid->target(new Position(9,9)));
    }

    public function testTargetingHits()
    {
        // fill the grid ready to test hitting things
        $ship = new Ship(4, new Position(0, 0), Ship::DIR_HORIZONTAL);
        $this->grid->placeShip($ship);      // only need one to hit

        // we aim at somewhere with a ship - get the ship back.
        $ship = $this->grid->target(new Position(0,0));
        $this->assertInstanceOf('Battleships\Ship', $ship);
        $this->assertEquals(1, $ship->getHitCount());
        $this->assertEquals(25, $ship->getHitPercentage());

        $ship = $this->grid->target(new Position(1,0));
        $this->assertEquals(2, $ship->getHitCount());
        $this->assertEquals(50, $ship->getHitPercentage());
    }

    public function testTargetingDestroyedShip()
    {
        // fill the grid ready to test hitting things
        $ship = new Ship(2, new Position(0, 0), Ship::DIR_HORIZONTAL);
        $this->grid->placeShip($ship);      // only need one to hit

        // we aim at somewhere with a ship - get the ship back.
        $ship = $this->grid->target(new Position(0,0));
        $this->assertInstanceOf('Battleships\Ship', $ship);

        // see that one of the ship positions has been hit
        //#$this->grid->displayGrid();

        $ship = $this->grid->target(new Position(1,0));
        $this->assertTrue($ship->isDestroyed());
        $this->assertEquals(2, $ship->getHitCount());
        $this->assertEquals(100, $ship->getHitPercentage());

        // see that all of the ship positions have been hit
        //#$this->grid->displayGrid();
    }

    /**
     * Try to double-blow up a ship section!
     * 
     * @expectedException RuntimeException
     */
    public function testTargetSamePlaceTwice()
    {
        $ship1 = new Ship(2, new Position(0, 0), Ship::DIR_VERTICAL);
        $this->grid->placeShip($ship1);

        $ship = $this->grid->target(new Position(0,0));
        // and hit it again!
        $ship = $this->grid->target(new Position(0,0));
    }

    /**
     * So much easier to see something if you can 'see' what is going on.
     *
     * This was used while developing 'grid->displayGrid()'
     */
    public function testShowGrid()
    {
        $this->markTestSkipped();

        // put a few ships in place.
        foreach ($this->dataproviderPlaceShip() as $ship) {
            $this->grid->placeShip($ship[0]);
        }
        $this->grid->displayGrid();

        // hit the [0,0]
        $this->grid->target(new Position(0,0));

        // now we can see that the section at [0,0] has been hit
        $this->grid->displayGrid();

        $this->assertTrue(true);
    }
}
